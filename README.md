# png2studio_dat.html - Introduction

png2studio_dat.html is a standalone HTML/ECMAScript application to make a
custom sticker part for BrickLink Studio.

By “standalone application” we mean that the user just needs to open the file
png2studio_dat.html in their (recent) browser.  No web server needed.  No other
installation than downloading the files.


# Requirements

* NONE!


# Installation & Usage

* Installation: Download all the files on your computer.

* Usage: Open png2studio_dat.html in your browser and follow the instructions
  at the top.


Studio & BrickLink are trademarks of the LEGO Group, which does not sponsor,
endorse, or authorize this application.
